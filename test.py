import json
from typing import Dict

import yaml

# command_api_response_file_path = input("Enter file name/path for api response file(json file) ") # command_name_prefix
commands_yml_file = input("Enter yml file path/name ")  # fixed with context prefix
command_name = input("Enter Command name ")
# command_context_prefix = input("Enter Prefix for Command set in Context ")
command_api_response_json = command_name + ".json"
with open(command_api_response_json, ) as json_data:
    context_data_expected = json.load(json_data)

with open('mapping_fields.json', ) as json_data:
    mapping_fields_data = json.load(json_data)

with open(commands_yml_file, 'r') as stream:
    yaml_data = yaml.safe_load(stream)


# print(yaml_data)

class Commands:
    commands_list = []
    command_data_dict = {}
    commands_context_keys = []

    def iterate_dict(d: Dict) -> None:
        for k, v in d.items():
            if isinstance(v, dict):
                Commands.iterate_dict(v)
            elif k == 'commands':
                Commands.commands_list = v
                break
            elif k == 'name' and v == command_name:
                Commands.command_data_dict = d['outputs']
            # elif k == 'ouputs':
            #     Commads.commads_context_keys = v
            #     break

    def traverse(o):
        if isinstance(o, list):
            for value in o:
                Commands.iterate_dict(value)


Commands.iterate_dict(yaml_data)
Commands.traverse(Commands.commands_list)
prefix = context_data_expected["command_prefix"]
print(prefix)
context = []
list_of_expected_keys = []
for context_record in Commands.command_data_dict:
    list_of_expected_keys.append(context_record['contextPath'].replace(prefix, ''))
# print(list_of_expected_keys)


list_of_api_response_keys = []
records = context_data_expected['api_response']['records'][0]
for k, v in records.items():
    list_of_api_response_keys.append(k)
# print(list_of_api_response_keys)

for expected_key in list_of_expected_keys:
    if expected_key in list_of_api_response_keys:
        print(expected_key, 'is present in context')

    elif mapping_fields_data.get(expected_key) in list_of_api_response_keys:
        print(expected_key, 'is present in context')

    else:
        print(expected_key, 'field is not present in context')
